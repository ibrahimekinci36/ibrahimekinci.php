<?php

namespace App\Http\Controllers\Admin;
use App\Repositories\SiteSetingsRepository as SiteSetings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Hash;
class SiteSetingsController extends Controller
{
  /**
     * @var sitesetings
     */
    private $el;

    public function __construct(SiteSetings $el) {

        $this->sitesetings = $el;
    }

    public function index() {


          $data['sitesetingss'] = $this->sitesetings->all();
        return view('admin.sitesetings.index',$data);

        //     return view('admin.home.index',$this->sitesetings->all());
      // return $this->sitesetings->all();            
    //  return Response::json($this->sitesetings->all());
    }
     public function add()
      { 
        return view('admin.sitesetings.add');
      }
    public function addPost()
      {
      $el = array(
             'name' => Input::get('name'), 
             'value' => Input::get('value'), 
            );
        $sitesetings_id =  $this->sitesetings->create($el);
        return redirect('sitesetings')->with('message', 'Sitesetings successfully added');
    }
    public function delete($id)
    {   
        $sitesetings=Sitesetings::find($id);
        $sitesetings->delete();
        return redirect('admin/sitesetings')->with('message', 'Sitesetings deleted successfully.');
    }
    public function edit($id)
    {   
        $data['sitesetings']=  $this->sitesetings->find($id);
        return view('admin/sitesetings/edit',$data);
    }
    public function editPost()
    {   
        $id =Input::get('sitesetings_id');
        $sitesetings= $this->sitesetings->find($id);
                       
        $sitesetings_data = array(
          'name' => Input::get('name'), 
          'value' => Input::get('value'), 
        );
       $this->sitesetings->updateRich($sitesetings_data,$id);
        return redirect('admin/setings')->with('message', 'Sitesetings Updated successfully');
    }

    
    public function changeStatus($id)
    {   
        $sitesetings=Sitesetings::find($id);
        $sitesetings->status=!$sitesetings->status;
        $sitesetings->save();
        return redirect('admin/sitesetings')->with('message', 'Change sitesetings status successfully');
    }
     public function view($id)
    {   
        $data['sitesetings']=Sitesetings::find($id);
        return view('admin/sitesetings/view',$data);
        
    }
    
}
