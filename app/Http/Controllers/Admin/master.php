<?php

namespace App\Http\Controllers\Admin;
use App\Repositories\SiteSetingsRepository as SiteSetings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Hash;
class SiteSetingsController extends Controller
{
  /**
     * @var sitesetings
     */
    private $el;

    public function __construct(SiteSetings $el) {

        $this->sitesetings = $el;
    }


    public function index() {

    }
     public function add()
      { 
      
      }
    public function addPost()
      {
    }
    public function delete($id)
    {   
    
    }
    public function edit($id)
    {   
      
    }
    public function editPost()
    {   
      
    }
    public function changeStatus($id)
    {   
     
    }
     public function view($id)
    {   
      
    }
}
