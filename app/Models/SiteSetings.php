<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SiteSetings extends Model
{
protected $fillable = ['name', 'value', 'created_at', 'updated_at'];
}
