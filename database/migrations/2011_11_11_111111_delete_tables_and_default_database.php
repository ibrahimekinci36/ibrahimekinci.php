<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTablesAndDefaultDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('activity_log_types');
        Schema::dropIfExists('activity_logs');
        Schema::dropIfExists('logs');
        Schema::dropIfExists('branches');
        Schema::dropIfExists('addresses');
        Schema::dropIfExists('address_branches');
        Schema::dropIfExists('address_users');
        Schema::dropIfExists('contact_forms');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('media_types');
        Schema::dropIfExists('media');
        Schema::dropIfExists('media_localizations');
        Schema::dropIfExists('media_bags');
        Schema::dropIfExists('media_bag_localizations');
        Schema::dropIfExists('media_bag_items');
        Schema::dropIfExists('menus');
        Schema::dropIfExists('menu_localizations');
        Schema::dropIfExists('menu_items');
        Schema::dropIfExists('message_templates');
        Schema::dropIfExists('template_localizations');
        Schema::dropIfExists('post_types');
        Schema::dropIfExists('posts');
        Schema::dropIfExists('post_localizations');
        Schema::dropIfExists('post_activities');
        Schema::dropIfExists('post_announcements');
        Schema::dropIfExists('post_products');
        Schema::dropIfExists('comments');
        Schema::dropIfExists('comment_users');
        Schema::dropIfExists('comment_guests');
        Schema::dropIfExists('site_setings');
        Schema::dropIfExists('subscriber_guests');
        Schema::dropIfExists('subscriber_users');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
