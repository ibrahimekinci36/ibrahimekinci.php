<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
                        $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            
            // $table->increments('id');
            // $table->string('user_name', 30);
            // $table->string('firs_name', 30);
            // $table->string('last_name', 30);
            // $table->timestamp('birthday')->nullable();
            // $table->boolean('gender')->nullable();
            // $table->string('phone_number', 30);
            // $table->string('email')->unique();
            // $table->string('password');
            // $table->boolean('email_confirmed')->default(false);
            // $table->boolean('phone_number_confirmed')->default(false);
            // $table->boolean('lockout_enabled')->default(false);
            // $table->timestamp('lockoutEndDateUtc')->nullable();
            // $table->integer('AccessFailedCount')->default(0);


            // $table->rememberToken();
            // $table->timestamps();
            // $table->boolean('status')->default(true);
            // $table->boolean('deleted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
