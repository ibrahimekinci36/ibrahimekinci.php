<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('comment', 300);
            $table->ipAddress('ipAddress');

            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('deleted')->default(false);

            $table->integer('activity_log_type_id')->unsigned();
            $table->foreign('activity_log_type_id')->references('id')->on('activity_log_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
