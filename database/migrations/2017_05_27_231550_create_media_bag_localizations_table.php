<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaBagLocalizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_bag_localizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 150);
            $table->string('description', 300);
            $table->string('tag', 150);
            $table->string('display_url', 250);

            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('deleted')->default(false);
            $table->integer('media_bag_id')->unsigned();
            $table->foreign('media_bag_id')->references('id')->on('media_bags');
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_bag_localizations');
    }
}
