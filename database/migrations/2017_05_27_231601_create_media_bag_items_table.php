<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaBagItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_bag_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('display_url', 250);

            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('deleted')->default(false);

            $table->integer('media_id')->unsigned();
            $table->foreign('media_id')->references('id')->on('media');

            $table->integer('media_bag_id')->unsigned();
            $table->foreign('media_bag_id')->references('id')->on('media_bags');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_bag_items');
    }
}
