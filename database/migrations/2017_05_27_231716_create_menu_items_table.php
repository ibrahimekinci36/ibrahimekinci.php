<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('display_url', 255);
            $table->integer('row');
            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('deleted')->default(false);

            $table->integer('top_menu_item_id')->unsigned();
            $table->foreign('top_menu_item_id')->references('id')->on('menu_items');
            $table->integer('menu_localization_id')->unsigned();
            $table->foreign('menu_localization_id')->references('id')->on('menu_localizations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
