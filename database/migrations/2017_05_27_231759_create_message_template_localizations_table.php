<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTemplateLocalizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_template_localizations', function (Blueprint $table) {
           $table->increments('id');
            $table->string('title', 100);
            $table->string('short_detail', 300);
            $table->text('detail');
            $table->string('meta_title', 100);
            $table->string('meta_detail', 300);
            $table->string('meta_tags', 255);
            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('deleted')->default(false);

            $table->integer('message_template_id')->unsigned();
            $table->foreign('message_template_id')->references('id')->on('message_templates');
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_template_localizations');
    }
}
