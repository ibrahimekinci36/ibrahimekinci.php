<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTemplateLocalizationTypeEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('message_template_localization_type_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('subject', 30);
            $table->text('body');
            $table->text('bcc_email_addresses');

            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->boolean('deleted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_template_localization_type_emails');
    }
}
