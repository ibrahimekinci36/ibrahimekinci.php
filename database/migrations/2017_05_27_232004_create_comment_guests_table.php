<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_guests', function (Blueprint $table) {
            $table->integer('comment_id')->unsigned();
            $table->primary('comment_id');
            $table->foreign('comment_id')->references('id')->on('comments');

            $table->string('first_name', 30);
            $table->string('last_name', 30);
            $table->string('email', 30);
            $table->string('site_url', 30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_guests');
    }
}
