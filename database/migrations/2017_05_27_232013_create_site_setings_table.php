<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_setings', function (Blueprint $table) {
         $table->increments('id');
            $table->string('name', 30);
            $table->string('value', 30);
            // $table->integer('row');

            $table->timestamps();
            // $table->boolean('status')->default(true);
            // $table->boolean('deleted')->default(false);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_setings');
    }
}
