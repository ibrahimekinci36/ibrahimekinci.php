@extends('admin.layouts.app')

@section('title', 'Page Title')

@section('head')

@endsection

@push('scripts')

@endpush

@section('content')


<div class="container">
  <h2>Add Sitesetings</h2>  
<form role="form" method="post" action="{{Request::root()}}/admin/setings/addPost" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group">
    <label for="name">Name:</label>
    <input type="text" class="form-control" id="name" name="name">
  </div>
    <div class="form-group">
    <label for="value">Value:</label>
    <input type="text" class="form-control" id="value" name="value">
  </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
@endsection

