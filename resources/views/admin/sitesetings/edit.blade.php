@extends('admin.layouts.app')

@section('title', 'Page Title')

@section('head')

@endsection

@push('scripts')

@endpush

@section('content')
<div class="container">
 <ul class="nav navbar-nav">
		<li><a href="{{Request::root()}}/sitesetings">Manage Sitesetings</a></li>
		<li><a href="{{Request::root()}}/sitesetings/add-sitesetings">Add Sitesetings</a></li>
	  </ul>
  <h2>Update Sitesetings</h2>  
<form role="form" method="post" action="{{Request::root()}}/admin/setings/editPost" enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
 <input type="hidden" value="<?php echo $sitesetings->id ?>"   name="sitesetings_id">


	  <div class="form-group">
	<label for="name">Name:</label>
	<input type="text" value="<?php echo $sitesetings->name ?>" class="form-control" id="name" name="name">
  </div>
	<div class="form-group">
	<label for="value">Value:</label>
	<input type="text" value="<?php echo $sitesetings->value ?>" class="form-control" id="value" name="value">
  </div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection

