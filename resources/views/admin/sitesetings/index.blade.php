@extends('admin.layouts.app')

@section('title', 'Page Title')

@section('head')

@endsection

@push('scripts')
	{{-- <script src="/example.js"></script> --}}
@endpush

@section('content')


<div class="container">
      <ul class="nav navbar-nav">
        <li><a href="{{Request::root()}}/admin/setings">Manage Sitesetings</a></li>
        <li class="active" ><a href="{{Request::root()}}/admin/setings/add">Add Sitesetings</a></li>
      </ul>

@if(Session::has('message'))
  <div class="alert alert-success">
					<strong><span class="glyphicon glyphicon-ok"></span>{{  Session::get('message') }}</strong>
				</div>
@endif

@if(count($sitesetingss)>0)
  <table class="table table-hover">
	<thead>
	  <tr>
		<th>SL No</th>
		<th>name</th>
	   <th>Actions</th>
	  </tr>
	</thead>
	<tbody>
	<?php $i=1 ?>
@foreach($sitesetingss as $sitesetings)
	  <tr>
		<td>{{$i}} </td>
		<td> <a href="{{Request::root()}}/admin/sitesetings/view/{{$sitesetings->id}}" > {{$sitesetings->name }}</a> </td>

		<td>
		<a href="{{Request::root()}}/admin/setings/change-status/{{$sitesetings->id }}" > @if($sitesetings->status==0) {{"Activate"}}  @else {{"Dectivate"}} @endif </a>
		<a href="{{Request::root()}}/admin/setings/edit/{{$sitesetings->id}}" >Edit</a>
		<a href="{{Request::root()}}/admin/setings/delete/{{$sitesetings->id}}" onclick="return confirm('are you sure to delete')">Delete</a>
		</td>

	  </tr>
	<?php $i++;  ?>
	@endforeach
	</tbody>
  </table>
   @else
  <div class="alert alert-info" role="alert">
					<strong>No Sitesetingss Found!</strong>
				</div>
 @endif
</div>
@endsection

