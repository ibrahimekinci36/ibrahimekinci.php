<?php

//     // Controllers Within The "App\Http\Controllers\Admin" Namespace
//    // Route::get('/home', 'HomeController@index')->name('home');
//    // Route::get('/home', function () {return view('admin.home.index');});
// });

Auth::routes();
Route::group(['namespace' => 'Admin'], function () {
Route::group(['prefix' => 'admin'], function () {

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

// routes for sitesetings.
Route::group(array('prefix' => 'setings'), function()
{
Route::get('/', 'SitesetingsController@index');
Route::get('/add', 'SitesetingsController@add');
Route::post('/addPost', 'SitesetingsController@addPost');
Route::get('/delete/{id}', 'SitesetingsController@delete');
Route::get('/edit/{id}', 'SitesetingsController@edit');
Route::post('/editPost', 'SitesetingsController@editPost');
Route::get('/change-status-sitesetings/{id}', 'SitesetingsController@changeStatus');
Route::get('/view/{id}', 'SitesetingsController@view');
});
// end of sitesetings routes           

});// end group admin
});// end group namespace

Route::group(['namespace' => 'Site'], function () {
    Route::get('/', function () {
        return view('Site.Home.Index');
    });
    Route::get('/home', function () {
        return view('Site.Home.Index');
    });
});


